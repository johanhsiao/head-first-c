#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

/*function about signal*/
int catch_signal(int sig, void (*handler)(int))
{
	struct sigaction action; action.sa_handler = handler;
	sigemptyset(&action.sa_mask); action.sa_flags = 0;
	return sigaction (sig, &action, NULL);
}
void handle_shutdown(int sig)
{
	if (listener_d)
		close(listener_d);
	fprintf(stderr, "Bye!\n");
	exit(0);
}

/*function about error*/
void error(char *msg)
{
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

/*function about socket*/
int open_listener_socket()
{
	int s = socket(PF_INET, SOCK_STREAM, 0);
	if (s == -1)
		error("Can't open socket");
	return s;
}


int main()
{
	int i = 0, reuse = 1;
	char *advice[] = {
		"Take smaller bites\r\n",
		"Go for the tight jeans. No they do NOT make you look fat.\r\n",
		"One word: inappropriate\r\n",
		"Just for today, be honest. Tell your boss what you *really* think\r\n",
		"You might want to rethink that haircut\r\n"
	};
	int listener_d = socket(PF_INET, SOCK_STREAM, 0);
	if (listener_d == -1)
		error("无法创建套接字");
	if (setsockopt(listener_d, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(int)) == -1)
		error("无法设置套接字的“重新使用端口”选项");
	struct sockaddr_in name;
	name.sin_family = PF_INET;
	name.sin_port = (in_port_t)htons(51000);
	name.sin_addr.s_addr = htonl(INADDR_ANY);
	int c = bind (listener_d, (struct sockaddr *) &name, sizeof(name));
	if (c == -1)
		error("无法绑定端口");
	if (listen(listener_d, 10) == -1)
		error("无法监听");
	puts("Waiting for connection");
	while(1) {
		struct sockaddr_storage client_addr;
		unsigned int address_size = sizeof(client_addr);
		int connect_d = accept(listener_d, (struct sockaddr *)&client_addr, &address_size);
		if (connect_d == -1)
			error("无法打开副套接字");
		if (i > 4)
			i = 0;
		char *msg = advice[i];
		i++;
		send (connect_d, msg, strlen(msg), 0);
		close(connect_d);
	}
	return 0;
}