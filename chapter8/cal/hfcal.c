#include<stdio.h>
#include"hfcal.h"

void display_cal(float weight, float distance, float corff)
{
    printf("Weight: %3.2f lbs\n", weight);
    printf("Distance: %3.2f miles \n", distance);
    printf("Call burned: %4.2f cal \n", corff * weight * distance);
}
