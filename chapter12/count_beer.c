#include <stdio.h>
#include <sys/select.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
//#include <unistd.h>

int beers = 2000000;
pthread_mutex_t beers_lock = PTHREAD_MUTEX_INITIALIZER;

static void sleep_ms(unsigned int secs)
{
    struct timeval tval;
    tval.tv_sec=secs/1000;
    tval.tv_usec=(secs*1000)%1000000;
    select(0,NULL,NULL,NULL,&tval);
}

void error(char *msg)
{
	fprintf(stderr, "%s: %s\n", msg, strerror(errno));
	exit(1);
}

void* drink_lots(void *a)
{
	sleep_ms(350);
	int i;
	pthread_mutex_lock(&beers_lock);
	for (i = 0; i < 100000; i++)
	{
		//pthread_mutex_lock(&beers_lock);
		beers = beers - 1;
		//pthread_mutex_unlock(&beers_lock);
	}
	pthread_mutex_unlock(&beers_lock);
	printf("beers = %i\n", beers);
	return NULL;
}

// int drinks()
// {
// 	sleep_ms(350);
// 	int i;
// 	for (i = 0; i < 100000; i++)
// 	{
// 		beers = beers - 1;
// 	}
// 	printf("Drinks one round\n");
// 	if (beers < 0)
// 	{
// 		printf("Drinks all\n");
// 		return 2;
// 	}
// 	return 0;
// }

int main()
{
	pthread_t threads[20];
	int t;
	printf("%i bottles of beer on the wall\n", beers);
	for(t = 0; t < 20; t++)
	{
		if (pthread_create(&threads[t], NULL, drink_lots, NULL) == -1)
			error("无法创建线程");
	}
	void *result;
	for (t = 0; t < 20; t++)
	{
		pthread_join(threads[t], &result);
	}


	// int i,f;
	// f = 0;
	// while( f != 2 )
	// {
	// 	f = drinks();
	// }

	printf("There are now %i bottles of beer on the wall\n", beers);
	return 0;
}