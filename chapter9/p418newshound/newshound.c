#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

int main(int argc, char *argv[])
{
    char *feeds[] = {"https://daringfireball.net/feeds/main",
                    "http://www.rollingstone.com/rock.xml",
                    "http://eonline.com/gossip.xml"
                    };

    int times = 3;
    char *phrase = argv[1];
    int i;
    for(i = 0; i <times; i++){
        char var[255];
        sprintf(var, "RSS_FEED=%s", feeds[i]);
        char *vars[] = {var, NULL};
        if( execle("/opt/homebrew/bin/python3", "/opt/homebrew/bin/python3", "./rssgossip.py", phrase, NULL, vars) == -1){
            fprintf(stderr, "Can't run script: %s\n", strerror(errno));
            return -1;
        }
    }

    return 0;
}
